<?php
/**
 * Site Functions
 *
 * @package   TPC Site Functions
 * @author    Joey Hayes
 * @copyright 2015 Joey Hayes
 * @license   GPL-2.0+
 * @link      https://github.com/joeyred
 *
 * @wordpress-plugin
 * Plugin Name:       	 TPC Site Functions
 * Plugin URI:       	 https://bitbucket.org/bjoeyhayes/tpc-site-functions
 * Description:      	 All site-wide functions for taxpreparerconnections.com
 * Version:          	 0.4.1
 * Author:           	 Joey Hayes
 * Author URI:       	 https://github.com/joeyred
 * Text Domain:      	 tpc-site-functions
 * License:          	 GPL-2.0+
 * License URI:      	 http://www.gnu.org/licenses/gpl-2.0.txt
 * Domain Path:       	 /languages
 * Bitbucket Plugin URI: https://bitbucket.org/bjoeyhayes/tpc-site-functions
 */

/**
 * All Site Functions are Contained in included files in the "inc" directory.
 * If you need to add a brand new feature to the sites functionality, then
 * a new file should be created in the "inc" directory and included in this file.
 *
 * The methods by which these functions are hooked into WordPress either via
 * add_action() or add_filter() should be done in this file after the require().
 */

// CSS Class Input Field in All Widgets
require( 'inc/widget-css-class.php' );
add_action( 'widgets_init', array( 'tpc_widget_css', 'setup' ) );

// Add New User Contact Fields
require( 'inc/user_fields.php' );
add_filter( 'user_contactmethods', 'tpcsite_user_contact_fields', 10, 1 );

/**
 * Widgets
 */

// Widget - Clean Output
require( 'inc/widgets/clean-output-widget.php' );

// Widget - Verticle Info Box
require( 'inc/widgets/info_box_widget.php' );

// Widget - Verticle Info Box
require( 'inc/widgets/related_posts_widget.php' );

add_action( 'widgets_init', 'tpcsite_register_widgets' );
/**
 * Register and hook widgets into widget init
 *
 * @return void
 */
function tpcsite_register_widgets() {

	// Clean Output Widget
	register_widget( 'Tpcsite_Widget_Clean_Output' );

	// Virtical Info Box Widget
	register_widget( 'Tpcsite_Verticle_Info_Box' );

	// Related Posts Widget
	register_widget( 'Tpcsite_Related_Posts' );

}
