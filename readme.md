# Site Functions Plugin for TPC

This is the site functions plugin for Tax Preparer Connections. This plugin contains all site-specific functions.

## **Current Version** 0.4.1

## Main Features

### CSS Class Input on All Widgets

This feature adds an extra field in widget forms that allows for the addition of CSS classes for styling purposes. Most notably, adding Foundation Framework classes for easy structure.

### Social Media User Fields

New social media input fields added to user profiles on WordPress user edit page.

## Custom Widgets

### Clean HTML Output 

A widget for clean output of HTML with no containers or wrappers.

### Info Box

An Info Box with Font-Awesome icon, Title, and Body.

### Related Posts

Easy widget for display of related posts based on tags of the current post.

## Changelog

### 0.4.0

#### Features

- New Info Box Widget.
- New Related Posts Widget.

#### Changes

- Widgets registration hooked via single funtion.

### 0.3.0

#### Features

- New user profile fields for social media.

### 0.2.0

#### Features

- New Widget for basic clean output. 

### 0.1.0

#### Features

- CSS class field added to all widgets. 