<?php

/**
 * Add New Contact Fields to User Profile
 *
 * @since  0.3.0 
 */

/**
 * Filter function for contact fields
 * @param  array $contactmethods current fields
 * @return array                 ammended fields
 */
function tpcsite_user_contact_fields( $contactmethods ) {
    // Add Twitter
    $contactmethods['twitter']  = 'Twitter';
    // Add Facebook
    $contactmethods['facebook'] = 'Facebook';
    // Add Linkedin
    $contactmethods['linkedin'] = 'Linkedin';
    
    return $contactmethods;
}
