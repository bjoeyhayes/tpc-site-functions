<?php

/**
 * Add CSS Class Input Field On All Widgets
 * 
 * @package   TPC Site Functions
 * @subpackage Widget CSS Class Input
 * @author    Joey Hayes
 */

class tpc_widget_css {
	/**
	 * Initialize plugin
	 */
	public static function setup() {
		if ( is_admin() ) {
			// Add necessary input on widget configuration form
			add_action( 'in_widget_form', array( __CLASS__, '_input_fields' ), 10, 3 );
			// Save widget attributes
			add_filter( 'widget_update_callback', array( __CLASS__, '_save_attributes' ), 10, 4 );
		}
		else {
			// Insert attributes into widget markup
			add_filter( 'dynamic_sidebar_params', array( __CLASS__, '_insert_attributes' ) );
		}
	}
	/**
	 * Inject input fields into widget configuration form
	 *
	 * @since 0.1.0
	 * 
	 * @wp_hook action in_widget_form
	 *
	 * @param object $widget Widget object
	 * @return NULL
	 */
	public static function _input_fields( $widget, $return, $instance ) {
		$instance = self::_get_attributes( $instance );

		$id = esc_attr( $widget->get_field_id( 'widget-class' ) );
		$label = esc_html__( 'HTML Class(es)', 'widget-attributes' );
		$class = esc_attr( $instance['widget-class'] );
		$name = esc_attr( $widget->get_field_name( 'widget-class' ) );
		?>
		<p>
			<label for="<?php echo $id; ?>"><?php echo $label; ?></label>
			<input type="text" class="widefat" id="<?php echo $id; ?>" name="<?php echo $name; ?>" value="<?php echo $class; ?>" />
		</p>
		<?php
		
		return null;
	}
	/**
	 * Get default attributes
	 *
	 * @since 0.1.0
	 *
	 * @param array $instance Widget instance configuration
	 * @return array
	 */
	private static function _get_attributes( $instance ) {
		$instance = wp_parse_args(
			$instance,
			array(
				'widget-class' => '',
			)
		);
		return $instance;
	}
	/**
	 * Save attributes upon widget saving
	 *
	 * @since 0.1.0
	 * 
	 * @wp_hook filter widget_update_callback
	 *
	 * @param array  $instance     Current widget instance configuration
	 * @param array  $new_instance New widget instance configuration
	 * @param array  $old_instance Old Widget instance configuration
	 * @param object $widget       Widget object
	 * @return array
	 */
	public static function _save_attributes( $instance, $new_instance, $old_instance, $widget ) {
		$instance['widget-id'] = $instance['widget-class'] = '';
		// Classes
		if ( !empty( $new_instance['widget-class'] ) ) {
			$instance['widget-class'] = apply_filters(
				'widget_attribute_classes',
				implode(
					' ',
					array_map(
						'sanitize_html_class',
						explode( ' ', $new_instance['widget-class'] )
					)
				)
			);
		}
		else {
			$instance['widget-class'] = '';
		}
		return $instance;
	}
	/**
	 * Insert attributes into widget markup
	 *
	 * @since 0.1.0
	 * 
	 * @filter dynamic_sidebar_params
	 *
	 * @param array $params Widget parameters
	 * @return Array
	 */
	public static function _insert_attributes( $params ) {
		global $wp_registered_widgets;
		$widget_id  = $params[0]['widget_id'];
		$widget_obj = $wp_registered_widgets[ $widget_id ];
		if (
			!isset( $widget_obj['callback'][0] )
			|| !is_object( $widget_obj['callback'][0] )
		) {
			return $params;
		}
		$widget_options = get_option( $widget_obj['callback'][0]->option_name );
		if ( empty( $widget_options ) )
			return $params;
		$widget_num	= $widget_obj['params'][0]['number'];
		if ( empty( $widget_options[ $widget_num ] ) )
			return $params;
		$instance = $widget_options[ $widget_num ];

		// Classes
		if ( ! empty( $instance['widget-class'] ) ) {
			$params[0]['before_widget'] = preg_replace(
				'/class="/',
				sprintf( 'class="%s ', $instance['widget-class'] ),
				$params[0]['before_widget'],
				1
			);
		}
		return $params;
	}
}









