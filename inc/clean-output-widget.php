<?php 

/**
 * Create New Widget for Clean HTML Output
 *
 * @since  0.2.0 
 */

class Tpcsite_Widget_Clean_Output extends WP_Widget {

	// Set up widget name and details
	function tpcsite_widget_clean_output() {
		parent::__construct(
			'clean_output', 'TPC - HTML',
			array( 'description' => __( 'Outputs exact HTML entered.', 'tpc-site-functions' ), )
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	function widget( $args, $instance ) {
		//extract($args);

		$markup = $instance['markup']; // Store 'markup' value from $insatnce array into $markup
		$output = do_shortcode($markup); // Run $markup through do_shortcode function to allow use of shortcodes and store in $ouput
		echo $output; // display output processed markup to frontend
	}
	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		$instance['markup'] = $new_instance['markup'];

		return $instance;
	}
	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	function form( $instance ) {
		
		// Check Value
		if ( $instance ) {
		$markup = esc_attr($instance['markup']);
		} else { 
			$markup = '';
		}
		?>
		<p>
			<label for="<?php echo $this->get_field_id('markup'); ?>">
			<textarea class="widefat" id="<?php echo $this->get_field_id('markup'); ?>" name="<?php echo $this->get_field_name('markup'); ?>" type="text" rows="16" cols="20" value="<?php echo $markup; ?>"><?php echo $markup; ?></textarea>
		</p>
		<?php
	}
}

// function tpcsite_widget_clean_output_register_widget() {
// 	register_widget( 'Tpcsite_Widget_Clean_Output' );
// }

