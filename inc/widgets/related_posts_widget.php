<?php

/**
 * Related Posts Widget
 *
 * @since  0.4.0 
 */

class Tpcsite_Related_Posts extends WP_Widget {

	// Set up widget name and details
	function tpcsite_related_posts() {
		parent::__construct(
			'related_posts', // Base ID
			'TPC - Related Posts', // Name
			array( 
			'description' => __( 'Related Posts', 'tpc-site-functions' ), 
			) // Args
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	function widget( $args, $instance ) {

		extract($args);
		$title 	   = apply_filters( 'widget_title', $instance['title'] );
		$num_posts = $instance['num_posts'];
		$post_type = $instance['post_type'];

		global $post;

		$tags = wp_get_post_tags( $post->ID ); // Get tags and store in variable

		if($tags) {
			
	        foreach( $tags as $tag ) {

	            $tag_arr .= $tag->slug . ',';
	        }

	        $args = array(
	        	'post_type' => $post_type,
	            'tag' => $tag_arr,
	            'numberposts' => $num_posts,
	            'post__not_in' => array($post->ID)
	        );

	        $related_posts = get_posts( $args );

			$class = 'small-block-grid-1 medium-block-grid-2 large-block-grid-3';

			if( $related_posts ) {
				?>
				<div class="small-12 columns related-post-widget">

				<h3 class="widget-title"><?php echo $title; ?></h3>
				<ul class="<?php echo $class; ?>">

				<?php 
				foreach( $related_posts as $post ) : setup_postdata( $post ); 

				$thumbnail_src = wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) );
				if( has_post_thumbnail() ) {
			    	$card_bg = ' related-post-thumbnail" style="background-image: url(' . $thumbnail_src . ');"';
			    } else {
			    	$card_bg = ' image-placeholder"';
			    }
				?>
					
					<li class="related-post">
			          <a class="related-post-link-wrap" href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>">

			            <div class="related-post-outer <?php echo $card_bg; ?>">

			              <div class="related-post-inner text-center">

			                <h3 class="post-title" data-equalizer-watch><?php the_title(); ?></h3>

			                <hr />

			                <h6 class="post-time"><?php the_time('F j, Y'); ?></h6>

			                <h6 class="post-author">by <?php echo get_the_author();  ?></h6>

			              </div>

			              <div class="hover-overlay">

			                <span class="read-more-link">Read More <span class="fa fa-chevron-right"></span></span>

			              </div>
			              
			            </div>

			          </a>
			        </li>

				<?php 
				//}
				endforeach; 
				
				?>
				</ul>
				</div>
				<?php
			}
			wp_reset_postdata();
		}
	}
	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	function update( $new_instance, $old_instance ) {

		$instance 			   = $old_instance;
		$instance['title'] 	   = strip_tags($new_instance['title']);
		$instance['num_posts'] = $new_instance['num_posts'];
		$instance['post_type'] = $new_instance['post_type'];

		return $instance;
	}
	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	function form( $instance ) {

		// Get all registered post types
		$post_types = get_post_types( '', 'names' );

		// Check Values
		if ( $instance ) {

			$title 	   = esc_attr($instance['title']);
			$num_posts = esc_attr($instance['num_posts']);
			$post_type = esc_attr($instance['post_type']);

		} else { 

			$title     = '';
			$num_posts = '';
			$post_type = '';
		}

		/**
		 * Title
		 */

		echo '<p><label>Title';
		echo '<input class="widefat"' 
		   . 'name="' . $this->get_field_name('title') . '"'
		   . 'type="text"'
		   . 'value="' . $title . '"'
		   . ' />'
		   ;
		echo '</label></p>';   

		/**
		 * Select Which Post Type to Display
		 */
		
		echo '<div style="'
		   . 'display: block;'
		   . 'width: 47%;'
		   . 'padding: 0 1%;' 
		   . '">'
		   ;
		echo '<label>Post Type'; // Label
		echo '<select name="' . $this->get_field_name('post_type') . '" type="text">';

		// Option loop
		foreach ( $post_types as $post_name ) {

		   echo '<option value="' . $post_name . '"'; // Option value

		   // Add 'selected' attribute if stored instance matches option
		   if ( $post_type == $post_name ) { echo ' selected="selected"'; } else { echo ''; }

		   echo '>' . $post_name . '</option>'; // Close 

		}

		echo '</select></label></div>';

		/**
		 * Select Number of Posts to Display
		 */
		
		//$num_posts = 3;

		echo '<div style="'
		   //. 'display: block;'
		   //. 'width: 47%;'
		   //. 'padding: 0 1%;' 
		   . '">'
		   ;
		echo '<label>Number of Posts Displayed'; // Label
		echo '<select name="' . $this->get_field_name('num_posts') . '" type="text">';

		// Option Loop
		for ($i = 1; $i <= 6; $i++ ) {

			echo '<option value="' . $i . '"'; // Option value

			// Add 'selected' attribute if stored instance matches option
		    if ( $num_posts == $i ) { echo ' selected="selected"'; } else { echo ''; }

		    echo '>' . $i . '</option>'; // Close
		}

		echo '</select></label></div>';
		
	}
}