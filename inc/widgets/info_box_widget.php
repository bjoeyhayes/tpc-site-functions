<?php

/**
 * Info Box Widget
 *
 * @since  0.4.0 
 */

class Tpcsite_Verticle_Info_Box extends WP_Widget {

	// Set up widget name and details
	function tpcsite_verticle_info_box() {
		parent::__construct(
			'vertical_info_box', // Base ID
			'TPC - Vertical Info Box', // Name
			array( 'description' => __( 'Info Box widget with icon, title, and bodytext.', 'tpc-site-functions' ), ) // Args
		);
	}

	/**
	 * Front End Markup
	 * 
	 * @param  array $args     before and after widget arguments
	 * @param  array $instance input field data
	 */
	function widget( $args, $instance ) {

		//extract($args);

		$classes = esc_attr($instance['classes']);
		$icon = esc_attr($instance['icon']);
		$title = esc_attr($instance['heading']);
		$bodytext = esc_attr($instance['bodytext']);

		echo '<div class="columns ' . $classes . ' tpc-info-box-widget" data-equalizer-watch>'; // Column Open
	    echo '<div class="info-box">'; // Info Box Open
	    echo '<div class="icon-wrap"><span class="fa fa-' . $icon . '"></span></div>'; // Icon
	    echo '<div class="box-inner">'; // Inner Box Open
	    echo '<h3>' . $title . '</h3>'; // Title
	    echo $bodytext; // Description
	    echo '</div></div></div>'; // box-inner, info-box, and column Close
	}
	
	/**
	 * Update Form Field Inputs on Save
	 * 
	 * @param  array $new_instance new input data
	 * @param  array $old_instance previously inputted data
	 * @return array               ammended data
	 */
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		// Fields
		$instance['classes']  = strip_tags($new_instance['classes']);
		$instance['icon'] 	  = strip_tags($new_instance['icon']);
		$instance['heading']  = strip_tags($new_instance['heading']);
		$instance['bodytext'] = strip_tags($new_instance['bodytext']);
		return $instance;
	}	

	/**
	 * Widget Form 
	 * 
	 * @param  array $instance form input
	 */
	function form( $instance ) {

		$icon = '';
		$heading = '';
	 	$bodytext = '';
	 	$classes = '';

		// Check values
		if( $instance ) {
			$classes = esc_attr($instance['classes']);
			$icon = esc_attr($instance['icon']);
			$heading = esc_attr($instance['heading']);
			$bodytext = esc_textarea($instance['bodytext']);
		} 
		?>

		<p>
			<label for="<?php echo $this->get_field_id('classes'); ?>"><?php _e('CSS Classes:', 'tpc-site-functions'); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id('classes'); ?>" name="<?php echo $this->get_field_name('classes'); ?>" type="text" value="<?php echo $classes; ?>" />
		</p>

		<p>Copy and Paste the icon name from <a href="http://fortawesome.github.io/Font-Awesome/cheatsheet/" alt="Font Awesome Cheatsheet">here</a>. DO NOT include the begining "fa-" when copying.</p>
		<p>
			<label for="<?php echo $this->get_field_id('icon'); ?>"><?php _e('Icon:', 'tpc-site-functions'); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id('icon'); ?>" name="<?php echo $this->get_field_name('icon'); ?>" type="text" value="<?php echo $icon; ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('heading'); ?>"><?php _e('Heading:', 'tpc-site-functions'); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id('heading'); ?>" name="<?php echo $this->get_field_name('heading'); ?>" type="text" value="<?php echo $heading; ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('bodytext'); ?>"><?php _e('Body:', 'tpc-site-functions'); ?></label>
			<textarea class="widefat" rows="10" id="<?php echo $this->get_field_id('bodytext'); ?>" name="<?php echo $this->get_field_name('bodytext'); ?>"><?php echo $bodytext; ?></textarea>
		</p>	
		<?php 
	}
} 

